from unfurl.testing import isolated_lifecycle, Step, assert_no_mypy_errors
from unfurl.localenv import LocalEnv
import os
import pytest

def test_deploy():
    "Create a deployment in a new dashboard using the 'testing' deployment-blueprint" 
    init_args = f"clone {os.path.abspath('.')} --skeleton dashboard --use-deployment-blueprint testing test_deployment".split()
    list(isolated_lifecycle("", [Step("deploy")], env=dict(UNFURL_SKIP_SAVE="never"), init_args=init_args, job_args=["--dryrun", "test_deployment"]))

@pytest.mark.skipif(
    not assert_no_mypy_errors, reason="mypy not installed"
)
def test_mypy():
    print("checking", os.path.abspath("service_template.py"))
    # instantiate this to force the tosca_repositories package to be created
    LocalEnv("ensemble-template.yaml").get_manifest(skip_validation=True, safe_mode=True)
    assert_no_mypy_errors("service_template.py")
