# This file is included by ensemble-template.yaml
# Here we defined TOSCA types as Python instead of YAML, see
# https://github.com/onecommons/unfurl/blob/main/tosca-package/README.md

# Below are the steps needed to create a simple blueprint, edit or replace the example code here.

import unfurl
import tosca
from tosca import DEFAULT
from unfurl.configurators.templates.docker import unfurl_datatypes_DockerContainer
from unfurl.tosca_plugins import expr
from tosca_repositories.std.generic_types import WebApp, ContainerService

# 1. Declare the App, this has global blueprint properties and resources
# which show up in the UI as inputs, components, extras


# By deriving your App from WebApp
class MyApp(WebApp):
    name: str
    container: "MyAppContainer"


# 2. Declare the container and its environment variables

class MyAppContainer(ContainerService):
    # DEFAULT is a marker object to indicate that the field should be initialized with the default value.
    # (note that AppEnvVars isn't defined yet so we can't assign one as the default here)
    environment: "AppEnvVars" = DEFAULT
    container = unfurl_datatypes_DockerContainer(
        image="registry.gitlab.com/gitlab-org/project-templates/express/main:latest",
        ports=["5000:5000"],
    )


# Derive a class from unfurl.datatypes.EnvironmentVariables to statically type environment variables.
# When saved as environment variables, each data attribute will be coerced to a string or omitted if null
class AppEnvVars(unfurl.datatypes.EnvironmentVariables):
    # expr.get_env() to retrieve variables from deployment's environment
    NODE_ENV: str = expr.get_env("NODE_ENV", "development")
    # target can be null, so use fallback expr (which is evaluated at runtime)
    PORT: tosca.datatypes.NetworkPortDef = expr.fallback(
        MyAppContainer.portspecs[0].target, tosca.datatypes.NetworkPortDef(5000)
    )


# 3. Declare pre-configured resource templates that use these type definitions.

# This is done in YAML (see ensemble_template.yaml) because if the template are not fully defined as they needs to be completed by the user when deploying this blueprint.
# (Incomplete templates would be a syntax error in Python.)
# __root__ = MyApp("the_app", container=MyAppContainer())
